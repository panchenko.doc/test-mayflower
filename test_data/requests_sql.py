"""Тут хранятся все запросы, которые совершаем в базе данных"""

CUSTOMERS_ALL = 'SELECT * FROM Customers;'

SHOW_CUSTOMERS_LONDON = "SELECT * FROM Customers WHERE city = 'London';"

ADD_NEW_CUSTOMER = "INSERT INTO Customers (CustomerName,ContactName,Address,City,PostalCode,Country) " \
                   "VALUES ('fxjaDEms9c','Контакт','Адрес','Город','Индекс','Страна');"

SHOW_NEW_CUSTOMER = "SELECT * FROM Customers WHERE CustomerName = 'fxjaDEms9c' AND ContactName = 'Контакт' AND" \
                    " Address ='Адрес' AND City = 'Город' AND PostalCode = 'Индекс' AND Country = 'Страна';"

UPGRADE_CUSTOMER = "UPDATE Customers SET CustomerName = 'fxjaDEms9c', ContactName='Контакт', Address='Адрес'," \
                   "City='Город', PostalCode='Индекс', Country='Страна' WHERE CustomerName='Alfreds Futterkiste';"

DELETE_CUSTOMER = "DELETE FROM Customers WHERE City = 'México D.F.';"

SHOW_CUSTOMERS_MEXICO = "SELECT * FROM Customers WHERE City = 'México D.F.';"

