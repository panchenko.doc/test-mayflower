"""Файл, содержащий фикстуры, настройки и импорты, общие для всех тестов"""

import pytest
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope="function")
def browser():
    print("\nSetting up Chrome browser...")
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--window-size=1980,1024')
    options.add_argument('--headless')

    # следующий параметр тут для того, чтобы не ждать полной загрузки страницы перед выполнением теста локально,потому
    # что при низкой скорости интернета мы ждём загрузки 10-15 сек, а то и больше. С этим параметром мы не ждём полной
    # загрузки, а только загрузку DOM, и идём дальше. При запуске в Гитлабе и в Докере этой
    # проблемы не будет, и параметр можно убрать. Но при локальном запуске проблема есть.
    options.page_load_strategy = 'eager'

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)

    # Переход на указанный URL, добавил его сюда, потому что мы всегда переходим по одному и тому же урлу
    url = "https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all"
    driver.get(url)

    yield driver

    print("\nTearing down Chrome browser...")
    driver.quit()

