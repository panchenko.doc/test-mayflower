from pages.page_sql import SqlPage
from test_data.requests_sql import *


class TestSQL:
    def test_check_giovanni_rovelli(self, browser):
        page = SqlPage(browser)
        page.send_request(CUSTOMERS_ALL)
        page.check_giovanni_rovelli()

    def test_six_customers_in_london(self, browser):
        page = SqlPage(browser)
        page.send_request(SHOW_CUSTOMERS_LONDON)
        page.six_entry()

    def test_add_customer(self, browser):
        page = SqlPage(browser)
        page.send_request(ADD_NEW_CUSTOMER)
        page.show_result()
        page.send_request(SHOW_NEW_CUSTOMER)
        page.check_add_customer()

    def test_upgrade_customer(self, browser):
        page = SqlPage(browser)
        page.send_request(UPGRADE_CUSTOMER)
        page.show_result()
        page.send_request(SHOW_NEW_CUSTOMER)
        page.check_add_customer()

    def test_delete_customer_mexico(self, browser):
        page = SqlPage(browser)
        page.send_request(DELETE_CUSTOMER)
        page.show_result()
        page.send_request(CUSTOMERS_ALL)
        page.check_after_delete_mexico()

        