from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


RUN_SQL_BUTTON_SELECTOR = (By.CSS_SELECTOR, 'button.ws-btn')
TABLE_SELECTOR = (By.CSS_SELECTOR, ".ws-table-all.notranslate")
ROWS_SELECTOR = (By.TAG_NAME, "tr")
RESULT_SELECTOR = (By.CSS_SELECTOR, ".w3-white#divResultSQL")


class SqlPage(BasePage):
    def send_request(self, data):
        # Очищаем содержимое поля ввода
        self.browser.execute_script("window.editor.setValue('')")
        # Заполняем поле ввода данными
        self.browser.execute_script("window.editor.setValue(arguments[0])", data)
        self.browser.find_element(*RUN_SQL_BUTTON_SELECTOR).click()

    def wait_loading(self):
        # Ожидание появления значения 'ContactName' на странице, т.к. интерфейс грузится с задержкой, и тесты падают
        WebDriverWait(self.browser, 5).until(
            EC.presence_of_element_located((By.XPATH, "//table//th[contains(text(), 'ContactName')]")))

    """следующая функция check_giovanni_rovelli проверяет, что имя 'Giovanni Rovelli' находится именно в столбце имён, 
    а не в каком-либо другом, что нужный адрес находится именно в столбце адресов, и что имя и адрес соответствуют.
     Через UI сложнее в десятки раз, чем проверить то же самое, имея доступ к базе данных. 
    Я мог бы упростить задачу, и сделать запрос:
    SELECT * FROM Customers
    WHERE ContactName = 'Giovanni Rovelli' AND Address = 'Via Ludovico il Moro 22'; 
    и убедиться,что одно поле вывелось. Но в ТЗ написано, что нужно вывести всю таблицу и в ней проверить, так что GO"""

    def check_giovanni_rovelli(self):
        # Определение порядкового номера столбца "ContactName"
        self.wait_loading()
        contact_name_column_index = None
        headers = self.browser.find_elements(By.XPATH, "//table//th")
        for index, header in enumerate(headers, start=1):
            if header.text == "ContactName":
                contact_name_column_index = index
                break

        # Определение порядкового номера столбца "Address"
        address_column_index = None
        for index, header in enumerate(headers, start=1):
            if header.text == "Address":
                address_column_index = index
                break

        # Поиск значения 'Giovanni Rovelli' в столбце "ContactName"
        contact_name_elements = self.browser.find_elements(By.XPATH, f"//table//tr/td[{contact_name_column_index}]")
        contact_name_row_index = None
        for index, element in enumerate(contact_name_elements, start=1):
            if element.text == "Giovanni Rovelli":
                contact_name_row_index = index+1
                break

        # Проверка, что значение 'Giovanni Rovelli' находится в столбце "ContactName"
        assert contact_name_row_index is not None, "'Giovanni Rovelli' не находится в столбце 'ContactName'"

        # Поиск значения "Address" на той же строке
        address_element = self.browser.find_element(By.XPATH,
                                              f"//table//tr[{contact_name_row_index}]/td[{address_column_index}]")

        # Проверка, что значение 'Via Ludovico il Moro 22' находится в столбце "Address"
        assert address_element.text == 'Via Ludovico il Moro 22'

    def six_entry(self):
        # Поиск всех строк таблицы
        self.wait_loading()
        table = self.browser.find_element(*TABLE_SELECTOR)
        rows = table.find_elements(*ROWS_SELECTOR)

        # Проверка количества строк в таблице
        assert len(rows) == 7, "Количество строк в таблице не равно 6"

    def check_add_customer(self):
        # Поиск всех строк таблицы
        self.wait_loading()
        table = self.browser.find_element(*TABLE_SELECTOR)
        rows = table.find_elements(*ROWS_SELECTOR)

        # Проверка количества строк в таблице
        assert len(rows) == 2, "Данные не были добавлены"

    def show_result(self):
        wait = WebDriverWait(self.browser, 10)

        # Ждем, пока текст "You have made changes to the database." появится на странице
        wait.until(EC.text_to_be_present_in_element(
            (By.XPATH, "//*[contains(text(), 'You have made changes to the database.')]"), ""))

    def check_after_delete_mexico(self):
        # Поиск всех строк таблицы
        self.wait_loading()
        table = self.browser.find_element(*TABLE_SELECTOR)
        rows = table.find_elements(*ROWS_SELECTOR)

        # Проверка количества строк в таблице
        assert len(rows) == 87, "Данные не были удалены"

